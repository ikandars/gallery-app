<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $photos = DB::table('gallery')->get();

        return view('welcome', ['photos' => $photos]);
    }

    public function upload(Request $request)
    {
        // $path = $request->file('photo')->store('avatars');

        $path = Storage::putFile('gallery', $request->file('photo'));
        $url = Storage::url($path);

        DB::table('gallery')->insert([
            'title' => '',
            'url' => $url
        ]);

        return redirect('/');
    }
}
